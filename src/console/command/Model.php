<?php

namespace zbw\console\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Db;
use think\helper\Str;

class Model extends Command
{
    protected $suffix = '';// 表后缀
    protected $name = '';
    protected $hump_name = '';
    protected $id;

    protected function configure()
    {
        // 指令配置
        $this->setName('zbw:model')
            ->addArgument('name', Argument::REQUIRED, '表名称  ')
            ->setDescription('创建模型');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->name = trim($input->getArgument('name'));
        if (!$this->name) {
            $output->writeln("表名必填");
            exit;
        }

        $this->hump_name = Str::studly(trim($this->name));
        $this->id = $this->getMainId();
        $this->createFile();
        $output->writeln("success");
    }

    public function createFile()
    {
        $dao_path = app_path() . 'common/dao/';
        if (!is_dir($dao_path)) mkdir($dao_path, 0755, true);
        $model_path = app_path() . 'common/model/';
        if (!is_dir($model_path)) mkdir($model_path, 0755, true);
        $repositories_path = app_path() . 'common/repositories/';
        if (!is_dir($repositories_path)) mkdir($repositories_path, 0755, true);
        $base_dao_path = $dao_path . 'BaseDao.php';
        if (!is_file($base_dao_path)) {
            $base_dao = file_get_contents(__DIR__ . '/tpl/BaseDao.tpl');
            file_put_contents($base_dao_path, $base_dao);
        }
        $base_model_path = $model_path . 'BaseModel.php';
        if (!is_file($base_model_path)) {
            $base_model = file_get_contents(__DIR__ . '/tpl/BaseModel.tpl');
            file_put_contents($base_model_path, $base_model);
        }
        $base_repository_path = $repositories_path . 'BaseRepository.php';
        if (!is_file($base_repository_path)) {
            $base_repository = file_get_contents(__DIR__ . '/tpl/BaseRepository.tpl');
            file_put_contents($base_repository_path, $base_repository);
        }

        $table_dao_path = $dao_path . ($this->hump_name) . 'Dao.php';
        if (!is_file($table_dao_path)) {
            $table_dao = file_get_contents(__DIR__ . '/tpl/Dao.tpl');
            file_put_contents($table_dao_path, $this->replaceParams($table_dao));
        }
        $table_model_path = $model_path . ($this->hump_name) . 'Model.php';
        if (!is_file($table_model_path)) {
            $table_model = file_get_contents(__DIR__ . '/tpl/Model.tpl');
            file_put_contents($table_model_path, $this->replaceParams($table_model));
        }
        $table_repository_path = $repositories_path . ($this->hump_name) . 'Repository.php';
        if (!is_file($table_repository_path)) {
            $table_repository = file_get_contents(__DIR__ . '/tpl/Repository.tpl');
            file_put_contents($table_repository_path, $this->replaceParams($table_repository));
        }
        return true;
    }

    public function getMainId()
    {
        $arr = Db::name($this->name)->getFields();
        $i = 0;
        $rest = 'id';
        foreach ($arr as $key => $value) {
            if (!$i) $rest = $key;
            break;
        }
        return $rest;
    }

    public function replaceParams($str)
    {
        $arr = [
            'name' => $this->name,
            'hump_name' => $this->hump_name,
            'id' => $this->id
        ];
        foreach ($arr as $key => $value) {
            $str = str_replace(':' . $key, $value, $str);
        }
        return $str;
    }
}