<?php

namespace app\common\dao;


use app\common\model\:hump_nameModel;

class :hump_nameDao extends BaseDao
{

    protected function getModel(): string
    {
        return :hump_nameModel::class;
    }

    public function search(array $where)
    {
        return :hump_nameModel::getDB()->when(isset($where[':id']) && $where[':id'], function ($query) use ($where) {
            $query->where(':id', intval($where[':id']));
        });
    }

}
