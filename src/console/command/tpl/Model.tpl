<?php

namespace app\common\model;


class :hump_nameModel extends BaseModel
{

    public static function tablePk(): string
    {
        return ':id';
    }

    public static function tableName(): string
    {
        return ':name';
    }

}
