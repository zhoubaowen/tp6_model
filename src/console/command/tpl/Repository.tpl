<?php
namespace app\common\repositories;

use app\common\dao\:hump_nameDao;

class :hump_nameRepository extends BaseRepository
{

    public function __construct(:hump_nameDao $dao)
    {
        $this->dao = $dao;
    }

    public function lst(array $where, int $page, int $limit): array
    {
        $query = $this->dao->search($where);
        $count = $query->count($this->dao->getPk());
        $list = $query->page($page, $limit)->select();
        return compact('count', 'list');
    }
}
