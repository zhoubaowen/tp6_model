<?php
namespace zbw;

use zbw\console\command\Model;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(Model::class);
    }
}