# tp6_create_model

#### 介绍
tp6 使用
1.  引入：composer require zbw/model dev-master
2.  使用：
    1.  env 配置好数据库链接
    2.  php think zbw:model 表名（不带前缀） 如： php think zbw:model admin

3.  会在app目录下创建common目录，创建 dao ,model , repository 层

代码使用：
    在app -> controller -> index.php 中的 ：

    文件头部引入：use app\common\repositories\AdminRepository;

    index 方法：

    public function index()
    {
        $arr = app()->make(AdminRepository::class)->lst([], 1, 10);
        foreach ($arr['list'] as $key => $value) {
            dump($value->admin_nickname);
            // $value->admin_nickname = $value->admin_nickname.'_cs';
            // $value->save();
        }
        exit();
    }   
